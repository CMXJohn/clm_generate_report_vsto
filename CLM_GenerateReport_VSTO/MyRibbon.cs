﻿using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;

namespace CLM_GenerateReport_VSTO
{
    public partial class MyRibbon
    {
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(
                this.button1_Click);
        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            // Initialize excel objects
            Excel.Application _xlApp = Globals.ThisAddIn.Application;
            Excel.Workbook wb = _xlApp.ActiveWorkbook;
            Excel.Worksheet ws1 = wb.Sheets.Add();
            ws1.Activate();
            //ws1.Name = "GENERATED REPORT " + Globals.ThisAddIn.Application.UserName;

            

            try
            {
                Debug.Print("chart 2");

                // Get Range
                Excel.Worksheet ws2 = wb.Sheets["ECJ ISSUES"];
                var range = ws2.get_Range("C:C");
                long end = ws2.Cells[ws2.Rows.Count, 1].End(Excel.XlDirection.xlUp).Row - 1;
                range = ws2.get_Range("A1:M" + end.ToString());

                //Make Pivot Table
                var pch = wb.PivotCaches();
                Excel.PivotCache pc = pch.Create(Excel.XlPivotTableSourceType.xlDatabase, range);
                pc.RefreshOnFileOpen = true;
                //pc.RefreshPeriod = 1;
                Excel.PivotTable pvt = pc.CreatePivotTable(ws1.Range["A1"], "ECJ Pivot");

                // Count of each Label
                var statusfield = pvt.PivotFields("Status");
                var dataField = pvt.AddDataField(statusfield);
                dataField.Orientation = Excel.XlPivotFieldOrientation.xlDataField;
                dataField.Function = Excel.XlConsolidationFunction.xlCount;
                dataField.Caption = "Sum of Salary";
                statusfield.Orientation = Excel.XlPivotFieldOrientation.xlRowField;

                //Generate Chart
                Microsoft.Office.Interop.Excel.Worksheet nativeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet;
                Microsoft.Office.Tools.Excel.Worksheet sheet = Globals.Factory.GetVstoObject(nativeWorksheet);

                //Chart reference
                Microsoft.Office.Tools.Excel.Chart productsChart;

                //Add a Pie Chart
                Microsoft.Office.Interop.Excel.Range chartRange = sheet.Range["A1", "B5"];
                double top = chartRange.Top;
                double left = sheet.Range["C1", "C5"].Left;
                productsChart = sheet.Controls.AddChart(left, top, 330, 200, "ECJISSUES");
                productsChart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;

                //Set chart title
                productsChart.HasTitle = true;
                productsChart.ChartTitle.Text = "ECJ";

                //Gets the cells that define the data to be charted.
                productsChart.SetSourceData(chartRange);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws2);
            }
            catch { }

            try
            {
                // Get Range
                Excel.Worksheet ws2 = wb.Sheets["HPA ISSUES"];
                var range = ws2.get_Range("C:C");
                long end = ws2.Cells[ws2.Rows.Count, 1].End(Excel.XlDirection.xlUp).Row - 1;
                range = ws2.get_Range("A1:M" + end.ToString());

                //Make Pivot Table
                var pch = wb.PivotCaches();
                Excel.PivotCache pc = pch.Create(Excel.XlPivotTableSourceType.xlDatabase, range);
                pc.RefreshOnFileOpen = true;
                //pc.RefreshPeriod = 1;
                Excel.PivotTable pvt = pc.CreatePivotTable(ws1.Range["A17"], "HPA Pivot");

                // Count of each Label
                var statusfield = pvt.PivotFields("Status");
                var dataField = pvt.AddDataField(statusfield);
                dataField.Orientation = Excel.XlPivotFieldOrientation.xlDataField;
                dataField.Function = Excel.XlConsolidationFunction.xlCount;
                dataField.Caption = "Sum of Salary";
                statusfield.Orientation = Excel.XlPivotFieldOrientation.xlRowField;

                //Generate Chart
                Microsoft.Office.Interop.Excel.Worksheet nativeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet;
                Microsoft.Office.Tools.Excel.Worksheet sheet = Globals.Factory.GetVstoObject(nativeWorksheet);

                //Chart reference
                Microsoft.Office.Tools.Excel.Chart productsChart;

                //Add a Pie Chart
                Microsoft.Office.Interop.Excel.Range chartRange = sheet.Range["A17", "B22"];
                double top = chartRange.Top;
                double left = sheet.Range["C17", "C22"].Left;
                productsChart = sheet.Controls.AddChart(left, top, 330, 200, "HPAISSUES");
                productsChart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;

                //Set chart title
                productsChart.HasTitle = true;
                productsChart.ChartTitle.Text = "HPA";

                //Gets the cells that define the data to be charted.
                productsChart.SetSourceData(chartRange);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws2);
            }
            catch { }

            try
            {
                // Get Range
                Excel.Worksheet ws2 = wb.Sheets["IFS ISSUES"];
                var range = ws2.get_Range("C:C");
                long end = ws2.Cells[ws2.Rows.Count, 1].End(Excel.XlDirection.xlUp).Row - 1;
                range = ws2.get_Range("A1:M" + end.ToString());

                //Make Pivot Table
                var pch = wb.PivotCaches();
                Excel.PivotCache pc = pch.Create(Excel.XlPivotTableSourceType.xlDatabase, range);
                pc.RefreshOnFileOpen = true;
                //pc.RefreshPeriod = 1;
                Excel.PivotTable pvt = pc.CreatePivotTable(ws1.Range["M1"], "IFS Pivot");

                // Count of each Label
                var statusfield = pvt.PivotFields("Status");
                var dataField = pvt.AddDataField(statusfield);
                dataField.Orientation = Excel.XlPivotFieldOrientation.xlDataField;
                dataField.Function = Excel.XlConsolidationFunction.xlCount;
                dataField.Caption = "Sum of Salary";
                statusfield.Orientation = Excel.XlPivotFieldOrientation.xlRowField;

                //Generate Chart
                Microsoft.Office.Interop.Excel.Worksheet nativeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet;
                Microsoft.Office.Tools.Excel.Worksheet sheet = Globals.Factory.GetVstoObject(nativeWorksheet);

                //Chart reference
                Microsoft.Office.Tools.Excel.Chart productsChart;

                //Add a Pie Chart
                Microsoft.Office.Interop.Excel.Range chartRange = sheet.Range["M1", "N5"];
                double top = chartRange.Top;
                double left = sheet.Range["O1", "O5"].Left;
                productsChart = sheet.Controls.AddChart(left, top, 330, 200, "IFSISSUES");
                productsChart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;

                //Set chart title
                productsChart.HasTitle = true;
                productsChart.ChartTitle.Text = "IFS";

                //Gets the cells that define the data to be charted.
                productsChart.SetSourceData(chartRange);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws2);
            }
            catch { }

            try
            {
                // Get Range
                Excel.Worksheet ws2 = wb.Sheets["STO ISSUES"];
                var range = ws2.get_Range("C:C");
                long end = ws2.Cells[ws2.Rows.Count, 1].End(Excel.XlDirection.xlUp).Row - 1;
                range = ws2.get_Range("A1:M" + end.ToString());

                //Make Pivot Table
                var pch = wb.PivotCaches();
                Excel.PivotCache pc = pch.Create(Excel.XlPivotTableSourceType.xlDatabase, range);
                pc.RefreshOnFileOpen = true;
                //pc.RefreshPeriod = 1;
                Excel.PivotTable pvt = pc.CreatePivotTable(ws1.Range["M17"], "STO Pivot");

                // Count of each Label
                var statusfield = pvt.PivotFields("Status");
                var dataField = pvt.AddDataField(statusfield);
                dataField.Orientation = Excel.XlPivotFieldOrientation.xlDataField;
                dataField.Function = Excel.XlConsolidationFunction.xlCount;
                dataField.Caption = "Sum of Salary";
                statusfield.Orientation = Excel.XlPivotFieldOrientation.xlRowField;

                //Generate Chart
                Microsoft.Office.Interop.Excel.Worksheet nativeWorksheet = Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet;
                Microsoft.Office.Tools.Excel.Worksheet sheet = Globals.Factory.GetVstoObject(nativeWorksheet);

                //Chart reference
                Microsoft.Office.Tools.Excel.Chart productsChart;

                //Add a Pie Chart
                Microsoft.Office.Interop.Excel.Range chartRange = sheet.Range["M17", "N22"];
                double top = chartRange.Top;
                double left = sheet.Range["O17", "O22"].Left;
                productsChart = sheet.Controls.AddChart(left, top, 330, 200, "STOISSUES");
                productsChart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;

                //Set chart title
                productsChart.HasTitle = true;
                productsChart.ChartTitle.Text = "STO";

                //Gets the cells that define the data to be charted.
                productsChart.SetSourceData(chartRange);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws2);
            }
            catch { }

            

            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_xlApp);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wb);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws1);
            

        }
    }
}